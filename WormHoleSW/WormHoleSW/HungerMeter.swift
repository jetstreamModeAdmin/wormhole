//
//  HungerMeter.swift
//  WormHoleSW
//
//  Created by ranger on 3/9/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

import UIKit

protocol HungerMeterDelegate {
    
    func updateHungerToChild(hungerVW: HungerMeter, hungerAmount: CGFloat, theLabel: NSString)
}

class HungerMeter: UIView {
    
    var delegate:HungerMeterDelegate?
    var label:String?

    var _hungerFill = UIView()
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        _hungerFill.frame = CGRect(x: 1, y: 1, width: 4, height: 68)
        _hungerFill.backgroundColor = UIColor(red:47/255, green:200/255,blue:168/255,alpha:1.0)
        self.addSubview(_hungerFill)
    }
    
    func setHungerAmount(amt: CGFloat) {
        //println("hunger amt = \(amt)")
        
        var currentHeight: CGFloat = _hungerFill.frame.size.height
        
        var newHeight: CGFloat = currentHeight-amt
        
        if (newHeight <= 0) {
            newHeight = 0
        }
        
        self.animateBar(newHeight, hfill: _hungerFill)
        
        //tell childview about new height
        
        delegate?.updateHungerToChild(self, hungerAmount: newHeight, theLabel: label!)
    }
    
    func animateBar(amt: CGFloat, hfill: UIView) {
        
        UIView.animateWithDuration(1.0, delay: 0.0, options: nil, animations: {
            
            hfill.frame = CGRect(x: 1.0, y: 69.0, width: 4.0, height: -amt)
            
            }, completion: {
                (value: Bool) in
        })
    }
    
    func endLifeCheck() {
        self.animateBar(0, hfill: _hungerFill)
    }
    
    func resetLife() {
        self.animateBar(68, hfill: _hungerFill)
    }
    

}
