//
//  ChildView.swift
//  WormHoleSW
//
//  Created by ranger on 3/9/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

import UIKit

protocol ChildProtocol {
    func sendMessageToMom(childVW: ChildView, theMessage: NSString)
}

class ChildView: UIViewController, HungerMeterDelegate {
    
    var delegate:ChildProtocol?
    var label:String?
    
    //var childImage:UIImage
    //var _childImage = UIImageView()
    let childImage0 = UIImage(named: "owl0")
    let childImage1 = UIImage(named: "owl1")
    let childImage2 = UIImage(named: "owl2")
    let _childImage = UIImageView()
    
    let display = UILabel()
    
    //percentage hunger
    var hungerPercentage: CGFloat = 1
    
    let _myHungerMeter = HungerMeter()
    
    //timer
    var hungerTimer = NSTimer()
    
    init(childName : String) {
        super.init(nibName: nil, bundle: nil)

        label = childName
    }

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func communicationChannelDirect(message: NSString) {
        display.text = message
        
        _myHungerMeter.resetLife()
        
        self.fadeUpChild()
        
        if (hungerTimer.valid) {
            
        } else {
            hungerPercentage = 1
            self.startUPTimer()
        }
        
        
        
    }
    func fadeUpChild() {
        UIView.animateWithDuration(0.3, delay: 0.0, options: nil, animations: {
            
            self._childImage.alpha = 1
            
            self._childImage.frame = CGRect(x: 0, y: 0, width: 170/2, height: 190/2)
            
            }, completion: {
                (value: Bool) in
                
                self.scaleDownChild()
        })
        
    }
    
    func scaleDownChild() {
        UIView.animateWithDuration(0.6, delay: 0.0, options: nil, animations: {
            
            
            self._childImage.frame = CGRect(x: 0, y: 0, width: 150/2, height: 166/2)
            
            }, completion: {
                (value: Bool) in
                
        })
    }
    

    func updateHungerToChild(hungerVW: HungerMeter, hungerAmount: CGFloat, theLabel: NSString) {
        //println("amt in child from hm = \(hungerAmount)")

        var hungerTotal = _myHungerMeter.frame.size.height

        hungerPercentage = (hungerAmount/hungerTotal)*100

        _childImage.alpha = hungerAmount/hungerTotal

        if (_childImage.alpha == 0) {
            _childImage.alpha = 0.1
        }

        var hungerMessage: NSString
        
        if (hungerPercentage > 89 && hungerPercentage <= 100) {
            hungerMessage = "i'm full";
        } else if (hungerPercentage > 79 && hungerPercentage < 90) {
            hungerMessage = "just ate";
        } else if (hungerPercentage > 69 && hungerPercentage < 80) {
            hungerMessage = "i could eat";
        } else if (hungerPercentage > 59 && hungerPercentage < 70) {
            hungerMessage = "when is lunch?";
        } else if (hungerPercentage > 49 && hungerPercentage < 60) {
            hungerMessage = "french fries, yes.";
        } else if (hungerPercentage > 39 && hungerPercentage < 50) {
            hungerMessage = "low blood sugar :(";
        } else if (hungerPercentage > 29 && hungerPercentage < 40) {
            hungerMessage = "crap, so hungry";
        } else if (hungerPercentage > 19 && hungerPercentage < 30) {
            hungerMessage = "no love from mom";
        } else if (hungerPercentage > 9 && hungerPercentage < 20) {
            hungerMessage = "writing my will";
        } else if (hungerPercentage > 0 && hungerPercentage < 10) {
            hungerMessage = "pretty much dead";
        } else {
            hungerMessage = "R.I.P";

            _myHungerMeter.endLifeCheck()
            
            hungerTimer.invalidate()
        }
        
        display.text = hungerMessage

    }

    func startUPTimer() {
        //var randomInterval:NSTimeInterval = arc4random()%5
        let randomInterval = NSTimeInterval(CGFloat(Int(arc4random()%5)))

        hungerTimer = NSTimer.scheduledTimerWithTimeInterval(randomInterval, target: self, selector: Selector("timerFired"), userInfo: nil, repeats: true)
        
    }
    
    func timerFired() {
        //println("timer = \(label)")
        
        //println("received child message in mom = \(theMessage)")
        if (hungerPercentage > 0) {
            _myHungerMeter.setHungerAmount(5)
        } else {
            hungerTimer.invalidate()
        }
    }

/*
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
*/


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        if (label == "kid1") {
            _childImage.image = childImage0
        } else if (label == "kid2") {
            _childImage.image = childImage1
        } else {
            _childImage.image = childImage2
        }
                
        _childImage.frame = CGRect(x: 0, y: 0, width: 150/2, height: 166/2)
        self.view.addSubview(_childImage)


        
        display.frame = CGRect(x: 0, y: 90, width: 150, height: 30)
        display.text = "Missing Mom"
        //display.backgroundColor = UIColor.grayColor()
        display.textColor = UIColor(red:47/255, green:200/255,blue:168/255,alpha:1.0)
        display.textAlignment = .Left
        self.view.addSubview(display)
        
        _myHungerMeter.frame = CGRect(x: 0, y: 20, width: 6, height: 70)
        _myHungerMeter.label = label
        _myHungerMeter.delegate = self
        self.view.addSubview(_myHungerMeter)
        
        self.startUPTimer()
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        var theCry: NSString
        
        if (label == "kid1") {
            theCry = "owl 1 is starving"
        } else if (label == "kid2") {
            theCry = "owl 2 is hungry"
        } else if (label == "kid3") {
            theCry = "owl 3 must eat"
        } else {
            theCry = "random unknown cry"
        }
        
        delegate?.sendMessageToMom(self, theMessage: theCry)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    /*
    UILabel *display = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 90.0, 150, 30)];
    [display setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1.0f]];
    [display setTextColor:[UIColor blackColor]];
    [display setText:@"Missing mom"];
    [self.view addSubview:display];
    [self setChildLabel:display];
_myHungerMeter = [[HungerMeter alloc] initWithFrame:CGRectMake(0, 20, 6, 70)];
_myHungerMeter.delegate = self;
_myHungerMeter.label = _label;
[self.view addSubview:_myHungerMeter];
    
    @class ChildView;
    @protocol ChildDelegate <NSObject>
    
    -(void)childVC:(ChildView *)childVW sendMessageToMom:(NSString *)theMessage;
    
    @end
*/

}
