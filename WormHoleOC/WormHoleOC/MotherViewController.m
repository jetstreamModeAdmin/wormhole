//
//  ViewController.m
//  WormHoleOC
//
//  Created by ranger on 2/27/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import "MotherViewController.h"

// This is defined in Math.h
#define M_PI   3.14159265358979323846264338327950288   /* pi */

// Our conversion definition
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

@interface MotherViewController ()

@end

@implementation MotherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _pendulum = 0;
    
    _motherImage = [[UIImageView alloc] initWithFrame:CGRectMake(238/2, 40/2, 463/2, 725/2)];
    _motherImage.image = [UIImage imageNamed:@"mother"];
    [self.view addSubview:_motherImage];
    
    _cageImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 25, 165/2, 140/2)];
    _cageImage.image = [UIImage imageNamed:@"cage"];
    [self.view addSubview:_cageImage];
    
    _cageMouse0 = [[UIImageView alloc] initWithFrame:CGRectMake(50, 20, 41/2, 41/2)];
    _cageMouse0.image = [UIImage imageNamed:@"cageMouse"];
    [_cageImage addSubview:_cageMouse0];
    [self runInCage:_cageMouse0];
    
    _cageMouse1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 30, 41/2, 41/2)];
    _cageMouse1.image = [UIImage imageNamed:@"cageMouse"];
    [_cageImage addSubview:_cageMouse1];
    [self runInCage:_cageMouse1];
    
    _cageMouse2 = [[UIImageView alloc] initWithFrame:CGRectMake(25, 25, 41/2, 41/2)];
    _cageMouse2.image = [UIImage imageNamed:@"cageMouse"];
    [_cageImage addSubview:_cageMouse2];
    [self runInCage:_cageMouse2];
    
    //now add mice to cage
    
    
    _shadesImage = [[UIImageView alloc] initWithFrame:CGRectMake(110, 40, 290/2, 153/2)];
    _shadesImage.image = [UIImage imageNamed:@"shades"];
    //[_motherImage addSubview:_shadesImage];
    
    
    
    _momMessagePane = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 75, 375, 30)];
    //[_momMessagePane setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1.0f]];
    [_momMessagePane setTextColor:[UIColor blackColor]];
    _momMessagePane.textAlignment = NSTextAlignmentCenter;
    //[display setFont:Helvetica24];
    [_momMessagePane setText:@".zz.zzz.z..."];
    [self.view addSubview:_momMessagePane];
    
    //UIButton *feederOne = [UIButton buttonWithType:UIButtonTypeCustom];
    
    

    
    //lets create some kids

    _childView1 = [[ChildView alloc] init];
    _childView1.delegate = self;
    _childView1.label = @"kid1";
    [_childView1.view setFrame:CGRectMake(15, 300, 100, 148)];
    [self.view addSubview:_childView1.view];


    _childView2 = [[ChildView alloc] init];
    _childView2.delegate = self;
    _childView2.label = @"kid2";
    [_childView2.view setFrame:CGRectMake(200, 390, 100, 148)];
    [self.view addSubview:_childView2.view];


    _childView3 = [[ChildView alloc] init];
    _childView3.delegate = self;
    _childView3.label = @"kid3";
    [_childView3.view setFrame:CGRectMake(50, 450, 100, 148)];
    [self.view addSubview:_childView3.view];
    
    //dangle mouse
    _spaceFood = [[UIImageView alloc] initWithFrame:CGRectMake(148, 93, 68/2, 120/2)];
    _spaceFood.image = [UIImage imageNamed:@"food"];
    _spaceFood.layer.anchorPoint = CGPointMake(0.5f, 0.0f);
    _spaceFood.alpha = 1;
    [self.view addSubview:_spaceFood];
    
    [self rotateImage:_spaceFood duration:1.0 curve:UIViewAnimationCurveEaseOut degrees:45];

    
}

-(void)runInCage:(UIImageView *)cagedMouse {
    
    float randomX = (arc4random()%44)+1;
    int xPos = 5+randomX;

    srand48(time(0));
    double r = drand48();
    
    [UIView animateWithDuration:.3+r
                          delay:r
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [cagedMouse setFrame:CGRectMake(xPos, cagedMouse.frame.origin.y, cagedMouse.frame.size.width, cagedMouse.frame.size.height)];

                     }
                     completion:^(BOOL finished){
                         
                         [self runInCage:cagedMouse];
                     }];
}

- (void)rotateImage:(UIImageView *)image duration:(NSTimeInterval)duration
              curve:(int)curve degrees:(CGFloat)degrees
{
    
    float randRotation = arc4random()%30;
    
    if (_pendulum == 0) {
        _pendulum = 1;
        degrees = randRotation;
    } else {
        _pendulum = 0;
        degrees = -randRotation;
    }
    
    [UIView animateWithDuration:duration
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGAffineTransform transform =
                         CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
                         image.transform = transform;
                     }
                     completion:^(BOOL finished){
                         [self rotateImage:_spaceFood duration:1.0 curve:UIViewAnimationCurveEaseIn degrees:0];
                     }];
}

-(void)spawnFood {
    if (_spaceFood != nil) {
        [_spaceFood removeFromSuperview];
    }
    
    _spaceFood = [[UIImageView alloc] initWithFrame:CGRectMake(148, 93, 68/2, 120/2)];
    _spaceFood.image = [UIImage imageNamed:@"food"];
    _spaceFood.layer.anchorPoint = CGPointMake(0.5f, 0.0f);
    _spaceFood.alpha = 1;
    [self.view addSubview:_spaceFood];
}

-(void)sendFoodToKid:(CGPoint)dPoint withChildViewC:(ChildView *)theChild{
    //food
    [_momMessagePane setText:@"feeding"];
    
    [self spawnFood];
    
    //animate it

    [UIView animateWithDuration:0.6
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         //maybe.
                         //[_spaceFood setFrame:CGRectMake(dPoint.x+25, dPoint.y+40, _spaceFood.frame.size.width, _spaceFood.frame.size.height)];
                         [_spaceFood setFrame:CGRectMake(dPoint.x+25, dPoint.y+40, 10, 10)];
                         _spaceFood.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         [_spaceFood removeFromSuperview];
                         [theChild communicationChannelDirect:@"hello child. EAT!"];
                         [_momMessagePane setText:@".zz.zzz.z..."];
                         
                        [self spawnFood];
                         
                     }];

    
}


-(void)onFeedOneTap:(UIButton *)sender {
    CGPoint destPoint;
    destPoint.x = _childView1.view.frame.origin.x;
    destPoint.y = _childView1.view.frame.origin.y;
    [self sendFoodToKid:destPoint withChildViewC:_childView1];
}
-(void)onFeedTwoTap:(UIButton *)sender {
    CGPoint destPoint;
    destPoint.x = _childView2.view.frame.origin.x;
    destPoint.y = _childView2.view.frame.origin.y;
    [self sendFoodToKid:destPoint withChildViewC:_childView2];
}
-(void)onFeedThreeTap:(UIButton *)sender {
    CGPoint destPoint;
    destPoint.x = _childView3.view.frame.origin.x;
    destPoint.y = _childView3.view.frame.origin.y;
    [self sendFoodToKid:destPoint withChildViewC:_childView3];
}

//look for cries from kids
-(void)childVC:(ChildView *)childVW sendMessageToMom:(NSString *)theMessage {
    [_momMessagePane setText:theMessage];
    
    //wake up mom

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


