//
//  ChildView.h
//  WormHoleOC
//
//  Created by ranger on 2/27/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HungerMeter.h"

@class ChildView;
@protocol ChildDelegate <NSObject>

-(void)childVC:(ChildView *)childVW sendMessageToMom:(NSString *)theMessage;

@end

@interface ChildView : UIViewController <HungerMeterDelegate>

@property (nonatomic,weak) id <ChildDelegate> delegate;
@property (nonatomic, strong) UIImageView *childImage;
@property (nonatomic, strong) UIImageView *feedMeImage;
@property (nonatomic,strong) UILabel *childLabel;
@property (nonatomic,strong) NSString *label;
@property (nonatomic, strong) HungerMeter *myHungerMeter;
@property (nonatomic, strong) NSTimer *hungerTimer;
@property (assign) int hungerPercentage;

-(void)communicationChannelDirect:(NSString *)message;

@end
