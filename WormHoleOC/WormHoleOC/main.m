//
//  main.m
//  WormHoleOC
//
//  Created by ranger on 2/27/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
