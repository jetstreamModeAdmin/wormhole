//
//  HungerMeter.m
//  WormHoleOC
//
//  Created by ranger on 3/2/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import "HungerMeter.h"

#define UIColorWithRGB(_R_,_G_,_B_) [UIColor colorWithRed:(_R_/255.0) green:(_G_/255.0) blue:(_B_/255.0) alpha:1.0];

@implementation HungerMeter

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    _hungerFill = [[UIView alloc] initWithFrame:CGRectMake(1, 1, 4, 68)];
    _hungerFill.backgroundColor = UIColorWithRGB(47, 200, 168);
    [self addSubview:_hungerFill];
}

-(void)setHungerAmount:(float)amt {
        
    float currentHeight = _hungerFill.frame.size.height;
    
    float newHeight = currentHeight-amt;
    
    if (newHeight <= 0) {
        newHeight = 0;
    }
    
    [self animateBar:newHeight];

    if ([_delegate respondsToSelector:@selector(hungerMeterVW:hungerLevel:withLabelID:)]) {
        [_delegate hungerMeterVW:self hungerLevel:newHeight withLabelID:_label];
    }
}

-(void)resetLife {
    
    [self animateBar:68];
}
-(void)endLifeCheck {
    [self animateBar:0];
}

-(void)animateBar:(float)amt {
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_hungerFill setFrame:CGRectMake(1, 69, 4, -amt)];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}




@end
