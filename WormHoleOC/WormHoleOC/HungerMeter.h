//
//  HungerMeter.h
//  WormHoleOC
//
//  Created by ranger on 3/2/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HungerMeter;
@protocol HungerMeterDelegate <NSObject>

-(void)hungerMeterVW:(HungerMeter *)hungerMView hungerLevel:(float)hungerAmount withLabelID:(NSString *)theLabel;

@end

@interface HungerMeter : UIView


@property (nonatomic,weak) id <HungerMeterDelegate> delegate;
@property (nonatomic, strong) UIView *hungerFill;
@property (nonatomic,strong) NSString *label;

-(void)setHungerAmount:(float)amt;
-(void)resetLife;
-(void)endLifeCheck;

@end
