//
//  ChildView.m
//  WormHoleOC
//
//  Created by ranger on 2/27/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import "ChildView.h"

@implementation ChildView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hungerPercentage = 1;
    
    NSString *kidImage;
    
    if ([_label isEqualToString:@"kid1"]) {
        kidImage = @"owl0";
    } else if ([_label isEqualToString:@"kid2"]) {
        kidImage = @"owl1";
    } else if ([_label isEqualToString:@"kid3"]) {
        kidImage = @"owl2";
    } else {
        kidImage = @"owl0";
    }
    
    _childImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150/2, 166/2)];
    _childImage.image = [UIImage imageNamed:kidImage];
    //_childImage.alpha = 0.3;
    [self.view addSubview:_childImage];
    
    UILabel *display = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 80.0, 150, 30)];
    //[display setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1.0f]];
    [display setBackgroundColor:[UIColor clearColor]];
    [display setTextColor:[UIColor colorWithRed:47.0f/255.0f green:200.0f/255.0f blue:168.0f/255.0f alpha:1.0f]];
    [display setText:@"Missing mom"];
    [self.view addSubview:display];
    [self setChildLabel:display];
    
    _myHungerMeter = [[HungerMeter alloc] initWithFrame:CGRectMake(0, 10, 6, 70)];
    _myHungerMeter.delegate = self;
    _myHungerMeter.label = _label;
    [self.view addSubview:_myHungerMeter];
    
    _feedMeImage = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 40, 40)];
    _feedMeImage.image = [UIImage imageNamed:@"feedMe"];
    //[_childImage addSubview:_feedMeImage];
    
    UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onChildHit:)];
    
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer: singleTap];
    

    [self startUPTimer];
    
}


-(void)communicationChannelDirect:(NSString *)message {
    [_childLabel setText:message];
    
    [_myHungerMeter resetLife];
    [self fadeUpChild];
    
    if (_hungerTimer) {
        //timer is running fine
    } else {
        _hungerPercentage = 1;
        [self startUPTimer];
    }
}

-(void)fadeUpChild {
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _childImage.alpha = 1;
                         [_childImage setFrame:CGRectMake(0, 0, 170/2, 190/2)];
                     }
                     completion:^(BOOL finished){
                         [self scaleDownChild];
                     }];
}
-(void)scaleDownChild {
    [UIView animateWithDuration:0.6
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [_childImage setFrame:CGRectMake(0, 0, 150/2, 166/2)];
                     }
                     completion:^(BOOL finished){
                         //[self scaleDownChild];
                     }];
}


-(void)onChildHit:(UITapGestureRecognizer *)gr {
    
    NSString *theCry;
    
    if ([_label isEqualToString:@"kid1"]) {
        theCry = @"owl 1 is starving";
    } else if ([_label isEqualToString:@"kid2"]) {
        theCry = @"owl 2 is hungry";
    } else if ([_label isEqualToString:@"kid3"]) {
        theCry = @"owl 3 must eat";
    } else {
        theCry = @"random unknown cry";
    }
    
    if ([_delegate respondsToSelector:@selector(childVC:sendMessageToMom:)]) {
        [_delegate childVC:self sendMessageToMom:theCry];
    }
}
-(void)startUPTimer {
    //timer to monitor hunger
    float randomInterval = (arc4random()%5)+1;
    _hungerTimer = [NSTimer scheduledTimerWithTimeInterval:randomInterval
                                                    target:self
                                                  selector:@selector(timerFired:)
                                                  userInfo:nil
                                                   repeats:YES];
}

-(void)timerFired:(NSTimer *)theTimer
{
    if (_hungerPercentage > 0) {
        [_myHungerMeter setHungerAmount:5];
    } else {
        [_hungerTimer invalidate];
        _hungerTimer = nil;
    }
    
}

//listen for hunger levels
-(void)hungerMeterVW:(HungerMeter *)hungerMView hungerLevel:(float)hungerAmount withLabelID:(NSString *)theLabel {
    
    float hungerTotal = _myHungerMeter.frame.size.height;
    
    _hungerPercentage = (hungerAmount/hungerTotal)*100;
    
    _childImage.alpha = (hungerAmount/hungerTotal)+.1;
    
    NSString *hungerMessage;
    
    if (_hungerPercentage > 89 && _hungerPercentage <= 100) {
        hungerMessage = @"i'm full";
    } else if (_hungerPercentage > 79 && _hungerPercentage < 90) {
        hungerMessage = @"just ate";
    } else if (_hungerPercentage > 69 && _hungerPercentage < 80) {
        hungerMessage = @"i could eat";
    } else if (_hungerPercentage > 59 && _hungerPercentage < 70) {
        hungerMessage = @"when is lunch?";
    } else if (_hungerPercentage > 49 && _hungerPercentage < 60) {
        hungerMessage = @"french fries, yes.";
    } else if (_hungerPercentage > 39 && _hungerPercentage < 50) {
        hungerMessage = @"low blood sugar :(";
    } else if (_hungerPercentage > 29 && _hungerPercentage < 40) {
        hungerMessage = @"crap, so hungry";
    } else if (_hungerPercentage > 19 && _hungerPercentage < 30) {
        hungerMessage = @"no love from mom";
    } else if (_hungerPercentage > 9 && _hungerPercentage < 20) {
        hungerMessage = @"writing my will";
    } else if (_hungerPercentage > 0 && _hungerPercentage < 10) {
        hungerMessage = @"pretty much dead";
    } else {
        hungerMessage = @"R.I.P";
        [_myHungerMeter endLifeCheck];
    }
    
    [_childLabel setText:hungerMessage];
}
@end
