//
//  ViewController.h
//  WormHoleOC
//
//  Created by ranger on 2/27/15.
//  Copyright (c) 2015 ranger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildView.h"

@interface MotherViewController : UIViewController <ChildDelegate>

@property (nonatomic, strong) UIImageView *motherImage;
@property (nonatomic, strong) UIImageView *cageImage;
@property (nonatomic, strong) UIImageView *cageMouse0;
@property (nonatomic, strong) UIImageView *cageMouse1;
@property (nonatomic, strong) UIImageView *cageMouse2;
@property (nonatomic, strong) UIImageView *shadesImage;
@property (nonatomic,strong) UILabel *momMessagePane;

@property (nonatomic, strong) UIImageView *spaceFood;

@property (nonatomic,strong) ChildView *childView1;
@property (nonatomic,strong) ChildView *childView2;
@property (nonatomic,strong) ChildView *childView3;

@property (assign) int pendulum;


@end

